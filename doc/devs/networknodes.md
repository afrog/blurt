---
title:  Network Nodes
geometry: margin=2cm
---

# Network Nodes

These are some of the currently known network nodes available in the Blurt ecosystem, please commit any changes to this document should you know of any nodes not listed or any of the currently listed ones ceasing to function. 


| Node Url              | Operator  |  
|-----------------------|-----------|
| api.blurtworld.com    | @yehey    |
| api.blurt.tools       | @blurtdev |
| blurtd.privex.io      | privex.io |  
| rpc.blurt.buzz        | @ericet   | 
| rpc.blurt.world       | Core Team | 
| api.blurt.blog        | Core Team |

# Blocktivity.info api 
https://blocktivity.blurtopian.com/api/stats

Fetches virtual operations and transfers in the last hour

https://blocktivity.blurtopian.com/api/stats?chain=blurt&include_details=true

Verbose version showing individual operation count

# Global Properties Nodes



| Node Url                               | Operator  |
| ---------------------------------------|-----------|
| https://biapi.blurt-now.com/api/blurt  | @eastmael |
| https://api.blurt.buzz/api/methodName  | @ericet   |

**Instructions:**

* get global dynamic properties: https://api.blurt.buzz/api/getDynamicGlobalProperties 
* get chain properties: https://api.blurt.buzz/api/getChainProperties
* get account details: https://api.blurt.buzz/api/getAccounts?names[]=ericet
All the methods listed here are supported: https://developers.steem.io/apidefinitions/#apidefinitions-condenser-api

