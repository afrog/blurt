# Changelog
All notable changes to Blurt will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- new payout model by @rycharde
- integrated condenser into the main repository
- support and docs for compiling on macos
- CMakeLists.txt restructured

### Changed


### Removed
- os-specific code from CMakeLists.txt files
